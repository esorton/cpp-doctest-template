FROM docker.io/gcc:9

# We start with the desired native gcc compiler version.  As gcovr is python
# based, the simplest installation is via pip.  We need a python3 virtual
# environment to avoid "error: externally-managed-environment" errors when
# running pip as root without a virtual environment [1,2].
#
# [1] https://stackoverflow.com/questions/75602063/pip-install-r-requirements-txt-is-failing-this-environment-is-externally-manag/75696359#75696359
# [2] https://peps.python.org/pep-0668/

# Install the virtual environment via debian packages.
RUN apt-get update && apt-get install -y \
      python3-venv \
 && rm -rf /var/lib/apt/lists/* \
 && python3 -m venv /venv

# Setup the environment varibles required to use the python virtual
# environment created in the previous stage.
ENV VIRTUAL_ENV /venv
ENV PATH /venv/bin:$PATH

# Install pip.
RUN curl https://bootstrap.pypa.io/get-pip.py -o /tmp/get-pip.py \
 && python /tmp/get-pip.py --no-wheel \
 && rm /tmp/get-pip.py

# Finally, install gcovr.
RUN pip install --no-cache-dir gcovr
